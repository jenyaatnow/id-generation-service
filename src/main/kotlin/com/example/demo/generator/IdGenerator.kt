package com.example.demo.generator

import java.math.BigInteger

interface IdGenerator {
    fun next(): BigInteger
}
