package com.example.demo.generator

import java.math.BigInteger

data class IdJsonPojo(val id: BigInteger)
