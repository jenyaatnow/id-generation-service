package com.example.demo.generator

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class UniqueIdResource(
    private val idGenerator: IdGenerator
) {

    @GetMapping("/id")
    fun getUniqueId(): IdJsonPojo {
        return IdJsonPojo(idGenerator.next())
    }
}
