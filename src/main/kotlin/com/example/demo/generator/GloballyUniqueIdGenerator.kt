package com.example.demo.generator

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import java.math.BigInteger
import java.util.concurrent.atomic.AtomicLong

@Component
class GloballyUniqueIdGenerator(
    @Value("\${app.node-id}") private val nodeId: Int,
    @Value("\${app.id-length}") private val idLength: Int
) : IdGenerator {

    private val atomicLong = AtomicLong()

    override fun next(): BigInteger {
        val id = atomicLong.incrementAndGet()
        val globallyUniqueIdStr = String.format("${nodeId}%0${idLength}d", id)
        val globallyUniqueId = BigInteger(globallyUniqueIdStr)

        return globallyUniqueId
    }
}
