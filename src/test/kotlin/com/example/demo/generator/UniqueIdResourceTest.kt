package com.example.demo.generator

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.BDDMockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.math.BigInteger

@ExtendWith(SpringExtension::class)
@WebMvcTest(UniqueIdResource::class)
class UniqueIdResourceTest {

    @Autowired private lateinit var mockMvc: MockMvc

    @MockBean private lateinit var idGenerator: IdGenerator

    @Test
    fun `should return correct id json`() {
        BDDMockito.given(idGenerator.next()).willReturn(BigInteger.valueOf(1))

        mockMvc
            .perform(MockMvcRequestBuilders.get("/id").contentType(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.content().json("""{ "id": 1 }"""))
    }
}
