package com.example.demo.generator

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.math.BigInteger

class GloballyUniqueIdGeneratorTest {

    @Test
    fun `should generate sequential unique ids from 1`() {
        val testable = GloballyUniqueIdGenerator(0, 10)

        Assertions.assertEquals(BigInteger.valueOf(1), testable.next())
        Assertions.assertEquals(BigInteger.valueOf(2), testable.next())
        Assertions.assertEquals(BigInteger.valueOf(3), testable.next())
    }

    @Test
    fun `should generate sequential unique ids from 50000000001`() {
        val testable = GloballyUniqueIdGenerator(5, 10)

        Assertions.assertEquals(BigInteger.valueOf(50000000001), testable.next())
        Assertions.assertEquals(BigInteger.valueOf(50000000002), testable.next())
        Assertions.assertEquals(BigInteger.valueOf(50000000003), testable.next())
    }

}
